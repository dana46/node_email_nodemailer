const express = require('express');
const app = express();
var bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const _ = require('lodash');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended : true}));
// parse application/json
app.use(bodyParser.json());

const SEND_EMAIL = '/send/email';

app.post(SEND_EMAIL, (req, res) => {
  let body = _.pick(req.body, ['host', 'port', 'secure', 'from', 'pass', 'to', 'cc', 'bcc', 'subject', 'text', 'html', 'attachments', 'priority']);
  // creating a transporter using nodemailer
  let mailTransport = nodemailer.createTransport({
    host    : body.host,
    port    : body.port,
    secure  : body.secure,
    auth    : {
      user  : body.from,
      pass  : body.pass
    }
  });
  // creating mail options
  let mailOptions = {
    to      : body.to,
    cc      : body.cc,
    bcc     : body.bcc,
    subject : body.subject,
    text    : body.text,
    html    : body.html,
    attachments : body.attachments,
    priority    : body.priority
  }
  // sending mail with callback using transporter sendMail
  mailTransport.sendMail(mailOptions, (err, info) => {
    if(err) {
      // error response
      res.status(404).send(err);
    } else {
      // success response
      res.status(200).send({
        accepted  : info.accepted,
        rejected  : info.rejected,
        pending   : info.pending,
        response  : info.response
      });
    }
  })
});


app.listen(3000, function() {
  console.log('Running on 3000!');
});
