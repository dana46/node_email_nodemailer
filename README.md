# API to send mails using Nodemailer and Express.js

A simple application which accepts a `POST` request for mail configuration as JSON, sends mail and responses success or failure.

## Install express.js

```javascript
npm install express --save
```
Simple program to create and run server using express.
```javascript
const express = require('express');
const app = express();

app.listen(3000, function() {
  console.log('Running on 3000!');
});
```
## Install body-parser

We will be using `body-parser` for parsing of different types of data, application/x-www-form-urlencoded, application/json.

```javascript
npm install body-parser --save
```

Including body-parser with app.use
```javascript
var bodyParser = require('body-parser');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended : true}));
// parse application/json
app.use(bodyParser.json()
```
## Install nodemailer by npm

> Using `nodemailer` library for sending mail.

For more info on *_nodemailer_*, visit [here](https://nodemailer.com/about/).

```javascript
npm install nodemailer --save
```

## Building the API

We will be sending the below raw `POST` data request to the API.
```javascript
{
	"host" : "smtp.gmail.com",
	"port" : "465",
	"secure" : "true",
	"from" : "hello@gmail.com",
	"pass" : "giveYourPassword",
	"to"   : "world@gmail.com",
	"subject" : "Hello World!",
	"text" : "Hello Dummy Email!",
	"html" : "<h2>Hello</h2>",
	"priority" : "high"
}
```

## Making a post route
```javascript
const SEND_EMAIL = '/send/email';

app.post(SEND_EMAIL, (req, res) => {
  .
  .
  .
});
```

## Picking the JSON properties using `lodash` library.
```javascript
let body = _.pick(req.body, ['host', 'port', 'secure', 'from', 'pass', 'to', 'cc', 'bcc', 'subject', 'text', 'html', 'attachments', 'priority']);
```

## Creating a transporter using nodemailer

> `nodemailer` comes with a transporter, which binds our email configuration and makes it ready to be used to trigger emails.

```javascript
let mailTransport = nodemailer.createTransport({
  host    : body.host,
  port    : body.port,
  secure  : body.secure,
  auth    : {
    user  : body.from,
    pass  : body.pass
  }
});
```

## Building the mailOptions
```javascript
let mailOptions = {
  to      : body.to,
  cc      : body.cc,
  bcc     : body.bcc,
  subject : body.subject,
  text    : body.text,
  html    : body.html,
  attachments : body.attachments,
  priority    : body.priority
};
```

## Final STEP! Send Email.
```javascript
mailTransport.sendMail(mailOptions, (err, info) => {
  if(err) {
    // error response
    res.status(404).send(err);
  } else {
    // success response
    res.status(200).send({
      accepted  : info.accepted,
      rejected  : info.rejected,
      pending   : info.pending,
      response  : info.response
    });
  }
});
```

Try configuring different kinds of email services, `Zoho` for example.